import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import logo from '../../logo.svg';
import '../../App.css';

import { loginUser } from '../../actions';

class Home extends Component {

  componentDidMount() {}

  componentWillReceiveProps(newProps){
    console.log(newProps);
  }

  handleLoginClick = () => {
    this.props.loginUser();
  }

  render() {    
    return (
      <div className="App">
        <header className="App-header">
          <div>Demo Task</div>
        </header>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {    
  return {
    user : auth
  };
};

export default withRouter( connect( mapStateToProps, { loginUser } )(Home) );